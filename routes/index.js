// Main routes for app
module.exports = function (app, db, errors) {

  var fs = require('fs');

  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }
  app.get('/', function (req, res, next) {

    res.render('index');

  });

  app.get('/users', function (req, res, next) {
    db.query('SELECT * FROM tbl_contacts', [], function (err, rows) {
      if (err) {
        return res.status(500).json(errors.DB_QUERY_ERROR);
      }
      if (rows.length > 0) {
        return res.status(200).json(rows);
      } else {
        return res.status(404).json(errors.DATA_NOT_FOUND)
      }
    });
  });

  app.post('/users', function (req, res, next) {
    if (!req.body.name || !req.body.primary_contact_number) {
      return res.status(422).json(errors.MANDATORY_FIELDS);
    }
    if (req.body.picture) {
      var currenttimestamp = new Date().getTime();
      var picture = req.body.picture;
      req.body.picture = guid() + currenttimestamp + ".png";
      fs.writeFile("Profile/" + req.body.picture, picture, 'base64', function (err) {
        if (err) {
          return res.status(500).json(errors.SERVER_ERROR);
        } else {
          db.query('INSERT INTO tbl_contacts SET ?', req.body, function (err, result) {
            if (err) {
              return res.status(500).json(errors.DB_QUERY_ERROR);
            }
            return res.status(200).json(errors.OK);
          });
        }
      });
    } else {
      db.query('INSERT INTO tbl_contacts SET ?', req.body, function (err, data) {
        if (err) {
          return res.status(500).json(errors.DB_QUERY_ERROR);
        }
        var result = errors.OK;
        result.id = data.insertId
        return res.status(200).json(result);
      });
    }
  });

  app.put('/users/picture/:id', function (req, res, next) {
    if (req.body.picture) {
      var currenttimestamp = new Date().getTime();
      var picture = req.body.picture;
      req.body.picture = guid() + currenttimestamp + ".png";
      fs.writeFile("Profile/" + req.body.picture, picture, 'base64', function (err) {
        if (err) {
          return res.status(500).json(errors.SERVER_ERROR);
        } else {
          db.query('UPDATE tbl_contacts SET picture= ? where id = ?', [req.body.picture, req.params.id], function (err, result) {
            if (err) {
              return res.status(500).json(errors.DB_QUERY_ERROR);
            }
            return res.status(200).json(errors.OK);
          });
        }
      });
    } else {
      return res.status(422).json(errors.MANDATORY_FIELDS);
    }
  })

  app.put('/users/:id', function (req, res, next) {
    if (!req.body.name || !req.body.primary_contact_number) {
      return res.status(422).json(errors.MANDATORY_FIELDS);
    }
    db.query('UPDATE tbl_contacts SET ? where id = ?', [req.body, req.params.id], function (err, data) {
      if (err) {
        return res.status(500).json(errors.DB_QUERY_ERROR);
      }
      return res.status(200).json(errors.OK);
    });
  });

  app.delete("/users/:id", function (req, res, next) {
    db.query('Delete from tbl_contacts where id = ?', [req.params.id], function (err, data) {
      if (err) {
        return res.status(500).json(errors.DB_QUERY_ERROR);
      }
      return res.status(200).json(errors.OK);
    });
  });

};
